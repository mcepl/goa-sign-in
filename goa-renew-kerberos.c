/* gcc goa-renew-kerberos.c $(pkg-config --cflags --libs goa-1.0
 * goa-backend-1.0 glib-2.0)
 */

#include <glib.h>

#define GOA_API_IS_SUBJECT_TO_CHANGE
#include <goa/goa.h>
#define GOA_BACKEND_API_IS_SUBJECT_TO_CHANGE
#include <goabackend/goabackend.h>

int
main (void)
{
  GList *accounts = NULL;
  GList *l;
  GoaClient *client = NULL;

  client = goa_client_new_sync (NULL, NULL);
  if (client == NULL)
    {
      g_warning ("Unable to connect to GOA");
      goto out;
    }

  accounts = goa_client_get_accounts (client);
  for (l = accounts; l != NULL; l = l->next)
    {
      GoaAccount *account;
      GoaObject *obj = GOA_OBJECT (l->data);
      const gchar *provider_type;

      account = goa_object_peek_account (obj);
      provider_type = goa_account_get_provider_type (account);
      if (g_strcmp0 (provider_type, "kerberos") == 0)
	{
	  GError *error;
	  GoaProvider *provider;

	  provider = goa_provider_get_for_provider_type (provider_type);

	  error = NULL;
	  if (!goa_provider_refresh_account (provider,
					     client, obj, NULL, &error))
	    {
	      g_warning ("Unable to refresh account: %s", error->message);
	      g_error_free (error);
	    }
	}
    }

out:
  g_list_free_full (accounts, g_object_unref);
  g_clear_object (&client);
  return 0;
}

/* vim: set cino=>2s,{1s,n-s,^-s tw=78 ts=4 sts=2 sw=2 et cin: */
