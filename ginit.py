from gi.repository import Goa
error = 'Uknown'

client = Goa.Client.new_sync(None)
if not client:
    raise RuntimeError('Could not create GoaClient')


res = [acc for acc in
       [obj.get_account() for obj
        in client.get_accounts()]
       if 'Kerberos' in acc.get_property('provider-name')]

for acc in res:
    print('Account %s' % acc.get_property('presentation-identity'))
    acc.call_ensure_credentials_sync(None)
