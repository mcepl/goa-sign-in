from gi.repository import Goa

client = Goa.Client.new_sync(None)
if not client:
    raise RuntimeError('Could not create GoaClient')

for obj_proxy in client.get_accounts():
    account = obj_proxy.get_account()
    print('{} at {}'.format(
        account.get_property('presentation-identity'),
        account.get_property('provider-name'),
    ))
    #account.call_ensure_credentials_sync(None, None, error)


#      <method name="call_ensure_credentials_sync"
#              c:identifier="goa_account_call_ensure_credentials_sync"
#              throws="1">
#        <doc xml:whitespace="preserve">Synchronously invokes the &lt;link linkend="gdbus-method-org-gnome-OnlineAccounts-Account.EnsureCredentials"&gt;EnsureCredentials()&lt;/link&gt; D-Bus method on @proxy. The calling thread is blocked until a reply is received.
#
#See goa_account_call_ensure_credentials() for the asynchronous version of this method.</doc>
#        <return-value transfer-ownership="none" skip="1">
#          <doc xml:whitespace="preserve">%TRUE if the call succeded, %FALSE if @error is set.</doc>
#          <type name="gboolean" c:type="gboolean"/>
#        </return-value>
#        <parameters>
#          <instance-parameter name="proxy" transfer-ownership="none">
#            <doc xml:whitespace="preserve">A #GoaAccountProxy.</doc>
#            <type name="Account" c:type="GoaAccount*"/>
#          </instance-parameter>
#          <parameter name="out_expires_in"
#                     direction="out"
#                     caller-allocates="0"
#                     transfer-ownership="full">
#            <doc xml:whitespace="preserve">Return location for return parameter or %NULL to ignore.</doc>
#            <type name="gint" c:type="gint*"/>
#          </parameter>
#          <parameter name="cancellable"
#                     transfer-ownership="none"
#                     allow-none="1">
#            <doc xml:whitespace="preserve">A #GCancellable or %NULL.</doc>
#            <type name="Gio.Cancellable" c:type="GCancellable*"/>
#          </parameter>
#        </parameters>
#      </method>
