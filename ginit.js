var Goa = imports.gi.Goa;

var client = Goa.Client.new_sync(null);
if (!client) {
  throw new Error("Could not create GoaClient");
}


var accts = client.get_accounts().map(function (obj) {return obj.get_account();});
var res = accts.filter(function(x) {
    return (x.get_property('provider_name').indexOf('Kerberos') != -1);
});

res.forEach(function (acc) {
    print('Account ' + acc.get_property('presentation-identity'));
    acc.call_ensure_credentials_sync(null);
});
