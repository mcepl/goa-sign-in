# Requires: gnome-online-accounts-devel
all: *.c
	gcc -g -o goa-renew-kerberos goa-renew-kerberos.c \
		$(shell pkg-config --cflags --libs goa-1.0 goa-backend-1.0 glib-2.0)

clean:
	rm -fv *~ *.o goa-renew-kerberos
